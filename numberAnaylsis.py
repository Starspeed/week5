#Evan Kelly
#October 1, 2015
#Project 4

def numberAnalysis():
    list1 = []
    for x in range (20):
        number = int(input('Enter a random integer' + str(x + 1)))
        list1.append(number)
    print('The lowest number is ' + str(min(list1)))
    print('The highest number is ' + str(max(list1)))
    print('The sum of the numbers is ' + str(sum(list1)))
    print('The average of the numbers in the list is ' + str(sum(list1)/len(list1)))

numberAnalysis()

